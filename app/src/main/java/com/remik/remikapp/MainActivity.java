package com.remik.remikapp;

import android.graphics.PorterDuff;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends ActionBarActivity implements OnSeekBarChangeListener {

    private ArrayList<CharSequence> scrollbarButtonLabels;
    private ArrayList<Button> scrollbarButtons;
    private int previouslyPressedScrollbarButtonIndex;
    private final int ACTIVE_BUTTON_COLOR_OFFSET = 0xFF00FF00;
    private final int NO_BUTTON_ASSIGNED = -1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeTopScrollbar();
        initializeSeekBars();

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adp = new ArrayAdapter<CharSequence>(this, R.layout.spinner, scrollbarButtonLabels);
        adp.setDropDownViewResource(R.layout.spinner);
        spinner.setAdapter(adp);
    }

    private void initializeTopScrollbar() {
        initializeScrollbarButtons();
        initializeScrollbarNavigationButtons();
    }

    private void initializeScrollbarButtonLabels() {
        scrollbarButtonLabels = new ArrayList<CharSequence>();
        for (int i = 0; i < 20; ++i)
        {
            scrollbarButtonLabels.add(String.valueOf(i));
        }
    }

    private void initializeScrollbarButtons() {
        initializeScrollbarButtonLabels();
        scrollbarButtons = new ArrayList<Button>();
        previouslyPressedScrollbarButtonIndex = NO_BUTTON_ASSIGNED;

        LinearLayout topLayout = (LinearLayout) findViewById(R.id.top_bar);
        View.OnClickListener buttonOnClickBehavior = createBehaviorForScrollBarButtons();
        for (CharSequence buttonLabel: scrollbarButtonLabels) {
            Button btn = new Button(this);
            btn.setText(buttonLabel);
            btn.setOnClickListener(buttonOnClickBehavior);

            scrollbarButtons.add(btn);
            topLayout.addView(btn);
        }
    }

    private View.OnClickListener createBehaviorForScrollBarButtons() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < scrollbarButtons.size(); i++) {
                    if (v == scrollbarButtons.get(i)) {
                        clearPreviouslyPressedButton();
                        assignButtonPressed(i);
                        break;
                    }
                }
            }

            private void assignButtonPressed(int listIndex) {
                Button btn = scrollbarButtons.get(listIndex);
                btn.getBackground().setColorFilter(ACTIVE_BUTTON_COLOR_OFFSET, PorterDuff.Mode.MULTIPLY);
                previouslyPressedScrollbarButtonIndex = listIndex;

                HorizontalScrollView hsView = (HorizontalScrollView) findViewById(R.id.top_scroll);
                int middleOfScrollView = (int) ((listIndex + 0.5) * btn.getWidth()) - (hsView.getWidth() / 2);
                hsView.scrollTo(middleOfScrollView, 0);
            }

            private void clearPreviouslyPressedButton() {
                if (previouslyPressedScrollbarButtonIndex != NO_BUTTON_ASSIGNED) {
                    Button previouslyPressedScrollbarButton = scrollbarButtons.get(previouslyPressedScrollbarButtonIndex);
                    previouslyPressedScrollbarButton.getBackground().clearColorFilter();
                    previouslyPressedScrollbarButton.invalidate();
                    previouslyPressedScrollbarButtonIndex = NO_BUTTON_ASSIGNED;
                }
            }

        };
    }

    private void initializeScrollbarNavigationButtons() {
        Button prev = (Button) findViewById(R.id.top_button_prev);
        Button next = (Button) findViewById(R.id.top_button_next);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (previouslyPressedScrollbarButtonIndex > 0)
                    scrollbarButtons.get(previouslyPressedScrollbarButtonIndex - 1).performClick();

                /* Alternatively, scroll by fixed amount to the left
                //HorizontalScrollView hsView = (HorizontalScrollView) findViewById(R.id.top_scroll);
                //hsView.arrowScroll(View.FOCUS_LEFT); */
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (previouslyPressedScrollbarButtonIndex < (scrollbarButtons.size() - 1))
                    scrollbarButtons.get(previouslyPressedScrollbarButtonIndex + 1).performClick();

                /* Alternatively, scroll by fixed amount to the right
                HorizontalScrollView hsView = (HorizontalScrollView) findViewById(R.id.top_scroll);
                hsView.arrowScroll(View.FOCUS_RIGHT); */
            }
        });
    }

    private void initializeSeekBars() {
        LinearLayout seekBarLayout = (LinearLayout) findViewById(R.id.seekbar_layout);

        for (int i = 0; i < getRequiredNumberOfSeekBars(); i++) {
            LabelRangePair lrp = new LabelRangePair(String.valueOf(i), 10 - i);
            View seekBarView = getLayoutInflater().inflate(R.layout.seekbar_layout, null);

            TextView label = (TextView) seekBarView.findViewById(R.id.seekbar_label);
            label.setText(lrp.label);

            SeekBar seekBar = (SeekBar) seekBarView.findViewById(R.id.seekbar);
            seekBar.setMax(lrp.range);
            seekBar.setOnSeekBarChangeListener(this);
            seekBarLayout.addView(seekBarView);
        }
    }

    public int getRequiredNumberOfSeekBars() {
        return 3;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        View parentView = (View) seekBar.getParent();
        TextView seekBarValue = (TextView) parentView.findViewById(R.id.seekbar_value);
        seekBarValue.setText(String.valueOf(progress));

        /*
        for (View sbv: seekBarViews) {
            SeekBar tempSeekBar = (SeekBar) sbv.findViewById(R.id.seekbar);
            if (seekBar.equals(tempSeekBar)) {
                TextView seekBarValue = (TextView) sbv.findViewById(R.id.seekbar_value);
                seekBarValue.setText(String.valueOf(progress));
                break;
            }
        }
        */
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // empty on purpose
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        seekBar.setSecondaryProgress(seekBar.getProgress());
    }
}
