package com.remik.remikapp;

public class LabelRangePair {
    public String label;
    public int range;

    LabelRangePair(String label, int range) {
        this.label = label;
        this.range = range;
    }
}
